# Cermati - Frontend

Contains solution for frontend programming test for Cermati.

In order for the cookies to work, please run the program on a server.

Tested on Chrome, Firefox, and Android devices.

---

Author: Alya Putri